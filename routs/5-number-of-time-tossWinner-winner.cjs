const fs=require('fs');
const dataMatches=require('../data/dataMatches.json');
const express= require('express');
const router=express.Router();


let res={};
dataMatches.filter((element)=>{
    if(element.winner==element.toss_winner){
        let team=element.winner;
        if(res[team]==undefined){
            res[team]=1;
        }
        else{
            res[team]+=1;
        }
    }
})


router.get('/',(req,response)=>{
    response.json(res);
});

module.exports=router;