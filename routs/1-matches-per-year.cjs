const fs=require('fs');
const express= require('express');
const router=express.Router();
const dataMatches=require('../data/dataMatches.json');


let result={};
dataMatches.filter((element)=> {
    let year=element.season;
    if(result[year]==undefined){
        result[year]=1;
    }
    else{
        result[year]++;
    }
});

router.get('/',(req,res)=>{
    res.json(result);
});

module.exports=router;