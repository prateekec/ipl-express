const fs=require('fs');
const dataDeleveries=require('../data/dataDeleveries.json');
const express= require('express');
const router=express.Router();


let processed_data={};
let highest_dismissal=0;

dataDeleveries.filter((element)=>{
    if(element.player_dismissed.length >0 && element.fielder.length > 0){
        let player=element.player_dismissed;
        if(processed_data[player]==undefined){
            processed_data[player]=1;
            highest_dismissal=Math.max(processed_data[player],highest_dismissal);
        }
        else{
            processed_data[player]++;
            highest_dismissal=Math.max(processed_data[player],highest_dismissal);
        }
    }
})


let res=Object.keys(processed_data).filter((element)=>{
    return processed_data[element]==highest_dismissal;
})


router.get('/',(req,response)=>{
    response.json(res);
});

module.exports=router;