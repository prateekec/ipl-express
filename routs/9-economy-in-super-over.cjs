const fs=require('fs');
const dataDeleveries=require('../data/dataDeleveries.json');
const express= require('express');
const router=express.Router();


let processed_data={};

dataDeleveries.filter((element)=>{
    if(element.is_super_over=="1"){
        let player=element.bowler;
        let runs=parseInt(element.total_runs);
        let id =element.match_id;

        if(processed_data[player]==undefined){
            let obj={};
            obj['runs']=runs;
            obj['overs']=new Set();
            obj['overs'].add(id);
            processed_data[player]=obj;
        }
        else{
            processed_data[player]['runs']+=runs;
            processed_data[player]['overs'].add(id);
        }
        
    }
})


let best_economy=Number.MAX_SAFE_INTEGER;
Object.keys(processed_data).filter((player)=>{
    let runs=processed_data[player]['runs'];
    let overs=processed_data[player]['overs'].size;
    processed_data[player]=runs/overs;
    best_economy=Math.min(best_economy,processed_data[player]);
})


let res=[];
Object.keys(processed_data).filter((player)=>{
    if(processed_data[player]==best_economy){
        res.push(player);
    }
})

router.get('/',(req,response)=>{
    response.json(res);
});

module.exports=router;