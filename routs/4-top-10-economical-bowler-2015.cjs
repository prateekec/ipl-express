const fs=require('fs');
const dataMatches=require('../data/dataMatches.json');
const dataDeleveries=require('../data/dataDeleveries.json');
const express= require('express');
const router=express.Router();


let ids=new Set();

dataMatches.map((element)=>{
    if(element.season==2015){
        ids.add(element.id);
    }
})


let res={};

dataDeleveries.filter((element)=>{
    if(ids.has(element.match_id)){
        let id=element.match_id;
        let name_bowler=element.bowler;
        let runs=element.total_runs;

        if(res[name_bowler]==undefined){
            let obj={}
            obj['runs']=parseInt(runs);
            obj['overs']={};
            obj['overs'][id]=new Set();
            obj['overs'][id].add(element.over);
            res[name_bowler]=obj;
        }
        else{
            res[name_bowler].runs+=parseInt(runs);
            if(res[name_bowler]['overs'][id]==undefined){
                res[name_bowler]['overs'][id]=new Set();
                res[name_bowler]['overs'][id].add(element.over);
            }
            res[name_bowler]['overs'][id].add(element.over);
            
        }      
    }
})


Object.keys(res).filter((player)=>{
    let total_overs=0;
    Object.keys(res[player]['overs']).map((id)=>{
        res[player]['overs'][id]=res[player]['overs'][id].size;
        total_overs+=res[player]['overs'][id];
    })
    res[player]['overs']=total_overs;
    let temp_run=res[player]['runs'];
    let balls=res[player]['overs'];
    res[player]=temp_run/balls;
})


res=Object.entries(res);
res.sort((a,b)=>a[1]-b[1])
res=res.slice(0,10);

router.get('/',(req,response)=>{
    response.json(res);
});

module.exports=router;