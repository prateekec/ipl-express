const fs=require('fs');
const dataMatches=require('../data/dataMatches.json');
const express= require('express');
const router=express.Router();

let res={};

dataMatches.filter((element)=>{
    let year=element.season;
    let team=element.winner;
    if(res[year]==undefined){
        let obj={};
        obj[team]=1;
        res[year]=obj;
    }
    else{
        if(res[year][team]==undefined){
            res[year][team]=1;
        }
        else{
            res[year][team]++;
        }
    }
})

router.get('/',(req,response)=>{
    response.json(res);
});

module.exports=router;