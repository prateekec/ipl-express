const fs=require('fs');
const dataMatches=require('../data/dataMatches.json');
const dataDeleveries=require('../data/dataDeleveries.json');
const express= require('express');
const router=express.Router();


let ids={};

dataMatches.map((element)=>{
    ids[element.id]=element.season;
})


let res={};

dataDeleveries.filter((element)=>{
    let ball =false;
    let year=ids[element.match_id];

    if(element.noball_runs=="0" && element.wide_runs=="0"){
        ball=true;
    }
    if(res[element.batsman]==undefined){
        let obj={};
        obj['runs']=parseInt(element.batsman_runs);
        obj['balls']=0;
        if(ball){
            obj['balls']++;
        }
        let temp={};
        temp[year]=obj;
        res[element.batsman]=temp;
    }
    else{
        if(res[element.batsman][year]==undefined){
            let obj={};
            obj['runs']=parseInt(element.batsman_runs);
            obj['balls']=0;
            if(ball){
                obj['balls']++;
            }
            res[element.batsman][year]=obj;
        }
        else{
            res[element.batsman][year]['runs']+=parseInt(element.batsman_runs);
            if(ball){
                res[element.batsman][year]['balls']++;
            }
        }
    }
})


Object.keys(res).filter((player)=>{
    Object.keys(res[player]).map((year)=>{
        let temp=res[player][year]['runs']*100;
        let balls=res[player][year]['balls'];
        temp/=balls;
        res[player][year]=temp;
    })
})


router.get('/',(req,response)=>{
    response.json(res);
});

module.exports=router;