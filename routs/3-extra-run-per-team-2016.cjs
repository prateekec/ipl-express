const fs=require('fs');
const dataMatches=require('../data/dataMatches.json');
const dataDeleveries=require('../data/dataDeleveries.json');
const express= require('express');
const router=express.Router();

let ids=new Set();
dataMatches.map((element)=>{
    if(element.season==2016){
        ids.add(element.id);
    }
})


let res={};

dataDeleveries.filter((element)=>{
    if(ids.has(element.match_id)){
        let team=element.bowling_team;
        let runs=parseInt(element.extra_runs);
        if(res[team]==undefined){
            res[team]=runs;
        }
        else{
            res[team]+=runs;
        }
    }
})


router.get('/',(req,response)=>{
    response.json(res);
});

module.exports=router;