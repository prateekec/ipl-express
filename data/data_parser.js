const fs=require('fs');
const papa= require('papaparse');


function parser(path){
    const csvdata=fs.readFileSync(path,'utf-8');
    const jsonData=papa.parse(csvdata,{
        header: true,
        skipEmptyLines: true,
    });
    return jsonData.data;
}

let data=parser('./data/matches.csv');
data=JSON.stringify(data,null,2);
fs.writeFileSync('./data/dataMatches.json',data);

data=parser('./data/deliveries.csv');
data=JSON.stringify(data,null,2);
fs.writeFileSync('./data/dataDeleveries.json',data);