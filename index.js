const express = require('express');
const ipl_app = express();
const path =require('path');
const port =process.env.PORT || 8000;
ipl_app.use(express.json());


ipl_app.use('/problem_1',require('./routs/1-matches-per-year.cjs'));
ipl_app.use('/problem_2',require('./routs/2-matches-won-per-year-per-team.cjs'));
ipl_app.use('/problem_3',require('./routs/3-extra-run-per-team-2016.cjs'));
ipl_app.use('/problem_4',require('./routs/4-top-10-economical-bowler-2015.cjs'));
ipl_app.use('/problem_5',require('./routs/5-number-of-time-tossWinner-winner.cjs'));
ipl_app.use('/problem_6',require('./routs/6-highest-number-of-playerOfTheMatch-winner.cjs'));
ipl_app.use('/problem_7',require('./routs/7-batsman-strike-rate-each-season.cjs'));
ipl_app.use('/problem_8',require('./routs/8-player-dismissed-highest-number-of-times.cjs'));
ipl_app.use('/problem_9',require('./routs/9-economy-in-super-over.cjs'));

ipl_app.get('/',(req,res) => {
    res.sendFile('./templates/home.html', {
        root : __dirname,
    });
})



ipl_app.listen(port);
